import { Component } from "@angular/core";
import { Page } from "ui/page";
import { Color } from "color";

@Component({
    selector: "main",
    template: "<page-router-outlet></page-router-outlet>"
})
export class AppComponent {
    constructor(private page: Page) {}

    ngOnInit() {
        console.log('ngOnInit from AppComponent');
        
        // this.page.actionBarHidden = true;
        // this.page.backgroundSpanUnderStatusBar = true;
        // this.page.backgroundColor = new Color('#000000');
    }
}
