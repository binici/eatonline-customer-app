import { NgModule, LOCALE_ID, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { registerElement } from "nativescript-angular/element-registry";
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { AppComponent } from "./app.component";
import { SearchModule } from './pages/search/search.module';
import { RestaurantModule } from './pages/restaurant/restaurant.module';
import { CartModule } from './pages/cart/cart.module';

import { appRoutes } from "./app.routing";
import { setStatusBarColors } from "./shared";

import { reducers } from './shared/reducers';
import { CartService, CartEffects } from './shared';

setStatusBarColors();
registerElement("DropDown", () => require("nativescript-drop-down/drop-down").DropDown);

@NgModule({
    declarations: [
        AppComponent
    ],
    bootstrap: [AppComponent],
    imports: [
        NativeScriptModule,
        NativeScriptHttpModule,
        NativeScriptRouterModule,
        NativeScriptRouterModule.forRoot(appRoutes),
        // StoreModule.provideStore({ cart: CartReducer }),
        StoreModule.provideStore(reducers),
        EffectsModule.run(CartEffects),
        SearchModule,
        RestaurantModule,
        CartModule
    ],
    providers: [
        { provide: LOCALE_ID, useValue: "da-DK" },
        CartService,
        CartEffects
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class AppModule { }
