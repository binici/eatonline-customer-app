import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page";
import { ActionBar } from "ui/action-bar";
import { ItemEventData } from 'ui/list-view';
import * as application from "application";
import { Store } from '@ngrx/store';

import { BackendService, AppState, Restaurant } from '../../../shared';

@Component({
    moduleId: module.id,
    selector: 'search-result',
    templateUrl: './search-result.component.html',
    styleUrls: ['./search-result.component.css']
})
export class SearchResultComponent implements OnInit {
    public items: any;
    public title: string = 'Test';
    public zipCode: string = '';
    public statusBarHeight: number;
    public loading: boolean;

    constructor(
        private page: Page,
        private route: ActivatedRoute,
        private router: RouterExtensions,
        private backend: BackendService,
        private store: Store<AppState>
    ) {
        this.page.backgroundSpanUnderStatusBar = true;
        this.statusBarHeight = this.getStatusBarHeight();
    }

    ngOnInit() {
        console.log('SearchResultComponent');
        this.route.queryParams.subscribe(params => {
            console.log('SearchResultComponent', JSON.stringify(params));
            let zipCode = params['zipCode'];
            let actionBar = this.page.getViewById<ActionBar>('actionBar');
            actionBar.title = zipCode;
            this.zipCode = zipCode;

            this.loading = true;
            this.backend.getRestaurantsByZipcode(zipCode).subscribe(
                (data) => {
                    this.loading = false;
                    this.items = data.restaurants;
                },
                (error) => console.error(error),
                () => { this.loading = false; }
            )
        });
    }

    onItemTap(args: ItemEventData) {
        let item = this.items[args.index];
        console.log('SearchResultComponent, onItemTap', item.restaurant_id);
        this.router.navigate(['/restaurant', item.restaurant_id]);
    }

    goBack() {
        this.router.back();
    }

    fixImageUrl(imgUrl: string) {
        return imgUrl.replace('http://', 'https://');
    }

    private getStatusBarHeight() {
        if (application.android) {
            return 20;
        } else {
            return 0;
        }
    }
}