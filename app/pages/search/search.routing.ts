import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { SearchFormComponent } from './form/search-form.component';
import { SearchResultComponent } from './result/search-result.component';

const searchRoutes: Routes = [
    {
        path: 'search',
        children: [
            { path: '', redirectTo: 'form', pathMatch: 'full' },
            { path: 'form', component: SearchFormComponent },
            { path: 'result', component: SearchResultComponent }
        ]
    }
];
export const searchRouting: ModuleWithProviders = RouterModule.forChild(searchRoutes);