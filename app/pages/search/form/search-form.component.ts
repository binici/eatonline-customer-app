import { Component, OnInit } from '@angular/core';
import { TextField } from "ui/text-field";
import { Page } from "ui/page";
import { EventData } from "data/observable";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    moduleId: module.id,
    selector: 'search-form',
    templateUrl: './search-form.component.html',
    styleUrls: ['./search-form.component.css']
})
export class SearchFormComponent implements OnInit {
    constructor(
        private page: Page,
        private router: RouterExtensions
    ) {
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
    }

    ngOnInit() {

    }

    findRestaurants(args: EventData) {
        let tfZipCode = this.page.getViewById<TextField>('tfZipCode');
        this.router.navigate(['/search/result'], {
            queryParams: {
                'zipCode': tfZipCode.text
            },
            transition: {
                name: "slideLeft"
            }
        });
    }
}