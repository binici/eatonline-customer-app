import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";

import { searchRouting } from "./search.routing";
import { BackendService } from '../../shared';

import { SearchFormComponent } from './form/search-form.component';
import { SearchResultComponent } from './result/search-result.component';

@NgModule({
  imports: [
    NativeScriptModule,
    NativeScriptFormsModule,
    searchRouting
  ],
  declarations: [
    SearchFormComponent,
    SearchResultComponent
  ],
  providers: [
      BackendService
  ],
  schemas: [NO_ERRORS_SCHEMA]
})
export class SearchModule {
  constructor() {
    console.log('SearchModule.ctor');
  }
}