import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { ModalDialogService } from "nativescript-angular/modal-dialog";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';

import { restaurantRouting } from "./restaurant.routing";
import { BackendService } from '../../shared';

import { CartModule } from "../cart/cart.module";

import { RestaurantMainComponent } from './main/restaurant-main.component';
import { MenuDetailComponent } from './menu-detail/menu-detail.component';

@NgModule({
    imports: [
        NativeScriptModule,
        NativeScriptFormsModule,
        TNSFontIconModule.forRoot({
            'fa': 'font-awesome.css'
        }),
        CartModule,
        restaurantRouting
    ],
    declarations: [
        RestaurantMainComponent,
        MenuDetailComponent
    ],
    providers: [
        BackendService,
        ModalDialogService
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class RestaurantModule {
    constructor() {
        console.log('RestaurantModule.ctor');
    }
}