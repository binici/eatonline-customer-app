import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterExtensions } from "nativescript-angular/router";
import { PageRoute } from "nativescript-angular/router";
import { Page } from "ui/page";
import { ActionBar } from "ui/action-bar";
import { SegmentedBarItem } from "ui/segmented-bar";
import { ItemEventData } from "ui/list-view";
import * as application from "application";
import 'rxjs/add/operator/switchMap';
import { Store } from "@ngrx/store";
import { Observable, BehaviorSubject } from "rxjs/Rx";

import { ModalDialogService } from "nativescript-angular/directives/dialogs";
import { CartAddModal } from "../../cart/cart-add/cart-add.component";

import { BackendService, AppState, RestaurantActions, CartItem, Restaurant } from '../../../shared';

@Component({
    moduleId: module.id,
    selector: 'restaurant-main',
    templateUrl: './restaurant-main.component.html',
    styleUrls: ['./restaurant-main.component.css'],
})
export class RestaurantMainComponent implements OnInit {
    public id: number;
    public restaurant: Restaurant;
    public menus: any[];

    public cart$: BehaviorSubject<CartItem[]> = new BehaviorSubject<CartItem[]>([]);
    public cartQty$ = this.cart$.map((cart) => {
        return cart.reduce((prev, cur) => prev + cur.qty, 0);
    })
    public cartTotal$ = this.cart$.map((cart) => {
        return cart.reduce((prev, cur) => prev + cur.totalPrice, 0);
    });

    public activatedRoute: ActivatedRoute;
    public statusBarHeight: number;
    public listViewHeight: number;
    public loading: boolean;

    public tabs: Array<SegmentedBarItem>;
    public selectedIndex = 0;
    public visibility1 = true;
    public visibility2 = false;
    public visibility3 = false;

    constructor(
        public store: Store<AppState>,
        private page: Page,
        private route: PageRoute,
        private router: RouterExtensions,
        private backend: BackendService,
        private modal: ModalDialogService,
        private vcRef: ViewContainerRef
    ) {
        console.log('RestaurantMainComponent.ctor');

        this.page.backgroundSpanUnderStatusBar = true;
        this.statusBarHeight = this.getStatusBarHeight();

        // use switchMap to get the latest activatedRoute instance
        this.route.activatedRoute
            .switchMap(activatedRoute => {
                this.activatedRoute = activatedRoute;
                return activatedRoute.params;
            })
            .forEach((params) => {
                this.id = + params['id'];
                console.log('RestaurantMainComponent.ctor: id =', this.id);
            });

        this.tabs = [];
        for (let i = 1; i < 4; i++) {
            let tmpSegmentedBar: SegmentedBarItem = <SegmentedBarItem>new SegmentedBarItem();
            tmpSegmentedBar.title = "View " + i;
            this.tabs.push(tmpSegmentedBar);
        }
        this.selectedIndex = 0;
    }

    ngOnInit() {
        console.log('RestaurantMainComponent.ngOnInit');

        this.loading = true;
        this.backend.getRestaurant(this.id).subscribe(
            (restaurant) => {
                this.restaurant = restaurant.restaurantdetails[0];
                this.menus = restaurant.restaurantdetails[0].category;
                this.listViewHeight = 60 * this.menus.length;

                // Dispatch store action
                this.store.dispatch(RestaurantActions.setRestaurant(this.restaurant));

                let actionBar = this.page.getViewById<ActionBar>('actionBar');
                actionBar.title = this.restaurant.restaurant_name;
            },
            (error) => console.error(error),
            () => { this.loading = false; }
        );

        // Get cart from store
        this.store.select('cart').subscribe((cart: CartItem[]) => {
            if (cart) {
                this.cart$.next(cart);
            }
        })

        // this.page.on(Page.navigatingToEvent, (event) => {
        //     if (this.router.locationStrategy._getStates().length > 3) {
        //         while (this.router.locationStrategy._getStates().length > 3) {
        //             let state = this.router.locationStrategy.pop();
        //         }
        //     }
        // })

        // this.page.on(Page.navigatedToEvent, (event) => {
        //     if (this.router.locationStrategy._getStates().length > 3) {
        //         if (this.router.locationStrategy._isPageNavigatingBack) {
        //             this.router.locationStrategy._finishBackPageNavigation();
        //         }
        //     }
        // })
    }

    public onChange(value) {
        this.selectedIndex = value;
        switch (value) {
            case 0:
                this.visibility1 = true;
                this.visibility2 = false;
                this.visibility3 = false;
                break;
            case 1:
                this.visibility1 = false;
                this.visibility2 = true;
                this.visibility3 = false;
                break;
            case 2:
                this.visibility1 = false;
                this.visibility2 = false;
                this.visibility3 = true;
                break;
            default:
                break;
        }
    }

    public onMenuListItemTap(args) {
        let menu = this.menus[args.index];
        console.log("RestaurantMainComponent.onMenuListItemTap", menu.maincateid);

        if (menu.menu.length === 1) {
            // show add to cart modal
            this.showModal(menu.menu[0].id);
        } else {
            // otherwise, show the sub-menus
            this.router.navigateByUrl(`/restaurant/${this.restaurant.restaurant_id}/menu/${menu.maincateid}`);
        }
    }

    public gotoCart(args: ItemEventData) {
        this.router.navigateByUrl('/cart/view');
    }

    public showModal(menuId) {
        let options = {
            context: {
                resId: this.restaurant.restaurant_id,
                menuId: menuId
            },
            fullscreen: true,
            viewContainerRef: this.vcRef
        };
        console.log('showModal');
        this.modal.showModal(CartAddModal, options).then(res => {
            console.log(res);
        });
    }

    fixImageUrl(imgUrl: string) {
        return imgUrl ?
            imgUrl.replace('http://', 'https://') : '';
    }

    private getStatusBarHeight() {
        if (application.android) {
            return 20;
        } else {
            return 0;
        }
    }
}