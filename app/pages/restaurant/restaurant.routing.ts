import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { RestaurantMainComponent } from './main/restaurant-main.component';
import { MenuDetailComponent } from './menu-detail/menu-detail.component';

const restaurantRoutes: Routes = [
    {
        path: 'restaurant',
        children: [
            {
                path: ':id',
                children: [
                    { path: '', component: RestaurantMainComponent },
                    { path: 'menu/:menuId', component: MenuDetailComponent }
                ]
            }
        ]
    }
];
export const restaurantRouting: ModuleWithProviders = RouterModule.forChild(restaurantRoutes);