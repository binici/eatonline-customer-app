import { Component, OnInit, ViewContainerRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RouterExtensions } from "nativescript-angular/router";
import { PageRoute } from "nativescript-angular/router";
import { Page } from "ui/page";
import { ActionBar } from "ui/action-bar";
import { BackendService } from '../../../shared';
import * as application from "application";
import { Frame } from "ui/frame";
var frameModule = require("ui/frame");
var topmost = frameModule.topmost();
import 'rxjs/add/operator/switchMap';

import { ModalDialogService } from "nativescript-angular/directives/dialogs";
import { CartAddModal } from "../../cart/cart-add/cart-add.component";

declare class Restaurant {
    restaurant_name: string;
    restaurant_logo: string;
    category: any[];
}

declare class Menu {
    maincateid: number;
    catname: string;
    menu: any[];
}

declare class SubMenu {
    id: number;
    menu_name: string;
    menu_description: string;
    menu_addons: string;
    sizeoption: string;
}

@Component({
    moduleId: module.id,
    selector: 'menu-detail',
    templateUrl: './menu-detail.component.html',
    styleUrls: ['./menu-detail.component.css'],
})
export class MenuDetailComponent implements OnInit {
    public id: number;
    public menuId: number;
    public restaurant: Restaurant
    public mainMenu: Menu;
    public subMenu: SubMenu[];

    public statusBarHeight: number;
    public listViewHeight: number;
    public loading: boolean;

    constructor(
        private page: Page,
        private route: PageRoute,
        private router: RouterExtensions,
        private backend: BackendService,
        private modal: ModalDialogService,
        private vcRef: ViewContainerRef,
        private frame: Frame
    ) {
        this.page.backgroundSpanUnderStatusBar = true;
        this.statusBarHeight = this.getStatusBarHeight();

        // use switchMap to get the latest activatedRoute instance
        this.route.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.id = + params['id'];
                this.menuId = + params['menuId'];
                console.log('MenuDetailComponent.ctor', this.id, this.menuId);
            });
    }

    ngOnInit() {
        console.log('MenuDetailComponent.ngOnInit');

        this.loading = true;
        this.backend.getRestaurant(this.id).subscribe(
            (restaurant) => {
                this.restaurant = restaurant.restaurantdetails[0];
                this.mainMenu = this.restaurant.category.filter(
                    (menu) => +menu.maincateid == this.menuId
                )[0];
                this.subMenu = this.mainMenu.menu;
                this.listViewHeight = 80 * this.subMenu.length;

                let actionBar = this.page.getViewById<ActionBar>('actionBar');
                actionBar.title = this.mainMenu.catname;
            },
            (error) => console.error(error),
            () => { this.loading = false; }
        );
    }

    public onItemTap(args) {
        let subMenu = this.subMenu[args.index];
        console.log("Sub Menu Item Tapped: " + args.index, subMenu.sizeoption);

        let options = {
            context: {
                menuId: subMenu.id,
                menuSize: ''
            },
            fullscreen: true,
            viewContainerRef: this.vcRef
        };
        this.modal.showModal(CartAddModal, options).then(res => {
            console.log('Modal closed');
            // this.router.back();
            this.frame.goBack();
        })
    }

    fixImageUrl(imgUrl: string) {
        return imgUrl ?
            imgUrl.replace('http://', 'https://') : '';
    }

    private getStatusBarHeight() {
        if (application.android) {
            return 20;
        } else {
            return 0;
        }
    }
}