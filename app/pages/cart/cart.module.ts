import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { TNSFontIconModule } from 'nativescript-ngx-fonticon';
import { SlidesModule } from 'nativescript-ng2-slides';

import { cartRouting } from "./cart.routing";
import { BackendService } from '../../shared';

import { CartAddModal } from "./cart-add/cart-add.component";
import { CartMenuAddonComponent } from './cart-menu-addon/cart-menu-addon.component';
import { CartMenuSizeComponent } from './cart-menu-size/cart-menu-size.component';
import { CartConfirmComponent } from './cart-confirm/cart-confirm.component';
import { CartViewComponent } from './cart-view/cart-view.component';

@NgModule({
    imports: [
        NativeScriptModule,
        NativeScriptFormsModule,
        TNSFontIconModule.forRoot({
            'fa': 'font-awesome.css'
        }),
        SlidesModule,
        cartRouting
    ],
    declarations: [
        CartAddModal,
        CartMenuAddonComponent,
        CartMenuSizeComponent,
        CartConfirmComponent,
        CartViewComponent
    ],
    entryComponents: [CartAddModal],
    providers: [
        BackendService
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class CartModule {
    constructor() {
        console.log('CartModule.ctor');
    }
}
