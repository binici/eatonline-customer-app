import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageRoute, RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page";
import { ActionBar } from "ui/action-bar";
import { ListView, ItemEventData } from 'ui/list-view';
import { Store } from '@ngrx/store';
import { ObservableArray } from 'data/observable-array';
import { TNSFontIconService } from 'nativescript-ngx-fonticon';
import * as Rx from 'rxjs';
import * as _ from 'lodash';
import * as application from "application";

import { BackendService, AppState, Menu, MainAddon, SubAddon, MenuSize } from '../../../shared';

@Component({
    moduleId: module.id,
    selector: 'cart-menu-addon',
    templateUrl: './cart-menu-addon.component.html',
    styleUrls: ['./cart-menu-addon.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CartMenuAddonComponent implements OnInit {
    public paramMenuId: number;
    public paramMenuSize: string = '';

    public menu$: Rx.BehaviorSubject<Menu> = new Rx.BehaviorSubject<Menu>(null);

    public mainAddons$: Rx.Observable<MainAddon[]> = this.menu$.map((menu: Menu) => {
        return menu ? menu.mainAddons : [];
    });

    public menuSize$ = this.menu$.map((menu) => {
        return menu ? menu.menuSize : '';
    })

    public menuSizes$ = this.menu$.map((menu) => {
        return menu ? menu.menuSizes : [];
    })

    public menuPrice$ = Rx.Observable.combineLatest(this.menu$, this.menuSizes$,
        (menu, menuSizes) => {
            if (menu) {
                switch (menu.menuSize) {
                    case 'fixed':
                        return menu.menuPrice;
                    case 'size':
                    case 'slice':
                        return menuSizes.reduce((prev, cur) => {
                            return prev + (cur.checked ? cur.price : 0);
                        }, 0);
                }
            } else {
                return 0;
            }
        }
    );

    public sum$ = Rx.Observable.combineLatest(this.menuPrice$, this.mainAddons$,
        (menuPrice: number, mainAddons: MainAddon[]) => {
            return menuPrice + mainAddons.map((mainAddon) => {
                return mainAddon.subAddons.filter((subAddon) => subAddon.checked)
            }).map((subAddons) => {
                return _.sumBy(subAddons, (subAddon: SubAddon) => subAddon.price);
            }).reduce((prev, cur) => prev + cur, 0)
        }
    )

    public valid$ = this.mainAddons$.map((mainAddons) => {
        return mainAddons.map((mainAddon) => {
            return mainAddon.addonsCount === 0 || mainAddon.addonsCount === mainAddon.subAddons.reduce<number>((prev, cur) => {
                return prev + (cur.checked ? 1 : 0);
            }, 0)
        }).reduce((prev, cur) => {
            return prev && cur;
        }, true);
    });

    public statusBarHeight: number;
    public listViewHeight: number;
    public loading: boolean = true;

    constructor(
        private page: Page,
        private route: PageRoute,
        private router: RouterExtensions,
        private backend: BackendService,
        private store: Store<AppState>,
        private fonticon: TNSFontIconService
    ) {
        this.page.backgroundSpanUnderStatusBar = true;
        this.statusBarHeight = this.getStatusBarHeight();

        // use switchMap to get the latest activatedRoute instance
        this.route.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.paramMenuId = +params['menuId'];
                console.log('CartMenuAddonComponent.ctor', this.paramMenuId);
            });

        this.route.activatedRoute
            .switchMap(activatedRoute => activatedRoute.queryParams)
            .forEach((queryParams) => {
                this.paramMenuSize = queryParams['menuSize'];
                console.log('CartMenuAddonComponent.ctor', this.paramMenuSize);
            })
    }

    ngOnInit() {
        console.log('CartMenuAddonComponent.ngOnInit');

        this.loading = true;
        this.backend.getAddons(this.paramMenuId).subscribe(
            (data) => {
                let menu: Menu;
                data.Menu.forEach((item) => {
                    menu = {
                        menuId: +item.id,
                        resId: +item.restaurant_id,
                        catId: +item.menu_category,
                        menuType: item.menu_type,
                        menuName: item.menu_name.trim(),
                        menuDescription: item.menu_description,
                        menuPrice: parseFloat(item.menu_price),
                        menuSize: item.sizeoption,
                        menuSizes: item.sizeoption === 'size' ?
                            [
                                {
                                    id: 'small',
                                    name: 'Almindelig',
                                    price: parseFloat(item.pizza_small_value),
                                    checked: this.paramMenuSize === 'small'
                                },
                                {
                                    id: 'medium',
                                    name: 'Deep Pan',
                                    price: parseFloat(item.pizza_medium_value),
                                    checked: this.paramMenuSize === 'medium'
                                },
                                {
                                    id: 'large',
                                    name: 'Familie',
                                    price: parseFloat(item.pizza_large_value),
                                    checked: this.paramMenuSize === 'large'
                                }
                            ] : item.Slice.map((slice) => {
                                return {
                                    id: +slice.pizza_slice_id,
                                    name: slice.pizza_slice_name,
                                    price: parseFloat(slice.pizza_slice_price),
                                    checked: this.paramMenuSize === slice.pizza_slice_id
                                }
                            }),
                        mainAddons: item.MainAddon.map((mainAddon) => {
                            return {
                                id: +mainAddon.menuaddons_id,
                                name: mainAddon.mainaddonsname.trim(),
                                addonsCount: +mainAddon.mainaddonsnamecnt,
                                subAddons: mainAddon.SubAddon.map((subAddon) => {
                                    return {
                                        id: +subAddon.menuaddons_id,
                                        name: subAddon.subaddonsname.trim(),
                                        price: parseFloat(subAddon.menuaddons_price),
                                        checked: false
                                    }
                                })
                            }
                        }).sort((a: MainAddon, b: MainAddon) => b.addonsCount - a.addonsCount)
                    }
                })
                // items = items.sort((a: MainAddon, b: MainAddon) => a.multiChoice === b.multiChoice ? 0 : a.multiChoice ? 1 : -1)
                this.menu$.next(menu);

                let actionBar = this.page.getViewById<ActionBar>('actionBar');
                actionBar.title = data.Menu[0].menu_name;
            },
            (error) => console.error(error),
            () => { this.loading = false; }
        );
    }

    mainAddonHeader(mainAddon: MainAddon) {
        let prefix = `${mainAddon.name} `;
        switch (mainAddon.addonsCount) {
            case 0:
                return `${prefix} (valgfrit)`;
            case 1:
                return `${prefix} (påkrævet)`;
            default:
                return `${prefix} (${mainAddon.addonsCount} valg)`;
        }
    }

    onSizeOptionSelected(args: ItemEventData) {
        let menu = this.menu$.getValue();
        if (menu === null)
            return;

        let menuSize = menu.menuSizes[args.index];

        // clear selection
        menu.menuSizes.forEach((menuSize) => {
            menuSize.checked = false;
        })

        // check selected
        menuSize.checked = true;

        // Update observables
        this.menu$.next(menu);

        // Update ui
        (<ListView>args.view.parent).refresh();

        // Change state


        console.log('onSizeOptionSelected', args.index);
    }

    onSubAddonTapped(args: ItemEventData, mainAddonIndex: number) {
        let menu = this.menu$.getValue();
        if (menu === null)
            return;

        let mainAddon = menu.mainAddons[mainAddonIndex];
        let subAddon = mainAddon.subAddons[args.index];
        let noChecked = mainAddon.subAddons.reduce<number>((prev, cur) => {
            return prev + (cur.checked ? 1 : 0);
        }, 0);

        if (mainAddon.addonsCount === 0) {
            // non-mandatory addon
            subAddon.checked = !subAddon.checked;
        } else if (mainAddon.addonsCount === 1) {
            // clear checked, if addons-count equals 1
            mainAddon.subAddons.forEach((subAddon) => {
                subAddon.checked = false;
            })
            subAddon.checked = true;
        } else {
            if (noChecked === mainAddon.addonsCount) {
                subAddon.checked = false;
            } else {
                subAddon.checked = true;
            }
        }

        // Update observables
        this.menu$.next(menu);

        // Update ui
        (<ListView>args.view.parent).refresh();

        console.log('onSubAddonTapped', mainAddonIndex, args.index, subAddon.checked);
    }

    onContinueTap(args: ItemEventData) {
        console.log('onContinueTap');

        let menu = this.menu$.getValue();
        let ids = menu.mainAddons.map((mainAddon) => {
            return mainAddon.subAddons.filter((subAddon) => subAddon.checked)
        }).map((subAddons) => {
            return subAddons.reduce((prev, cur) => {
                return prev + ',' + cur.id
            }, '');
        });

        this.router.navigate(['/cart/add', this.paramMenuId, 'confirm'], {
            queryParams: {
                'menuSize': this.paramMenuSize,
                'menuAddons': ids
            }
        });
    }

    private getStatusBarHeight() {
        if (application.android) {
            return 20;
        } else {
            return 0;
        }
    }
}