import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageRoute, RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page";
import { ActionBar } from "ui/action-bar";
import { ListView, ItemEventData } from 'ui/list-view';
import { Store } from '@ngrx/store';
import { ObservableArray } from 'data/observable-array';
import { TNSFontIconService } from 'nativescript-ngx-fonticon';
import * as Rx from 'rxjs';
import * as _ from 'lodash';
import * as application from "application";

import { BackendService, AppState, Menu, MainAddon, SubAddon, MenuSize } from '../../../shared';

@Component({
    moduleId: module.id,
    selector: 'cart-menu-size',
    templateUrl: 'cart-menu-size.component.html',
    styleUrls: ['./cart-menu-size.component.css'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class CartMenuSizeComponent implements OnInit {
    public paramMenuId: number;
    public paramMenuSize: string = '';

    public menu$: Rx.BehaviorSubject<Menu> = new Rx.BehaviorSubject<Menu>(null);

    public mainAddons$: Rx.Observable<MainAddon[]> = this.menu$.map((menu: Menu) => {
        return menu ? menu.mainAddons : [];
    });

    public menuSize$ = this.menu$.map((menu) => {
        return menu ? menu.menuSize : '';
    })

    public menuSizes$ = this.menu$.map((menu) => {
        return menu ? menu.menuSizes : [];
    })

    public menuState$ = Rx.Observable.combineLatest(this.menuSize$, this.menuSizes$,
        (menuSize, menuSizes) => {
            let hasSelectedMenuSize = this.paramMenuSize && menuSizes.some((menuSize) => {
                return menuSize.checked;
            })

            if (menuSize === 'size') {
                return hasSelectedMenuSize ? 'selectAddon' : 'selectSize'
            } else {
                return 'selectAddon';
            }
        }
    )

    public menuPrice$ = Rx.Observable.combineLatest(this.menu$, this.menuState$, this.menuSizes$,
        (menu, menuState, menuSizes) => {
            if (menu) {
                return menuSizes.reduce((prev, cur) => {
                    return prev + (cur.checked ? cur.price : 0);
                }, 0);
            } else {
                return 0;
            }
        }
    );

    public hasSelectedMenuSize$ = this.menuSizes$.map((menuSizes) => {
        return menuSizes.some((menuSize) => {
            return menuSize.checked;
        })
    })

    public hasAddons$ = this.mainAddons$.map((menuAddons) => {
        return menuAddons.length > 0;
    })

    public sum$ = Rx.Observable.combineLatest(this.menuPrice$, this.mainAddons$,
        (menuPrice: number, mainAddons: MainAddon[]) => {
            return menuPrice + mainAddons.map((mainAddon) => {
                return mainAddon.subAddons.filter((subAddon) => subAddon.checked)
            }).map((subAddons) => {
                return _.sumBy(subAddons, (subAddon: SubAddon) => subAddon.price);
            }).reduce((prev, cur) => prev + cur, 0)
        }
    )

    public valid$ = this.hasSelectedMenuSize$.map((hasSelectedMenuSize) => {
        return hasSelectedMenuSize;
    });

    public statusBarHeight: number;
    public listViewHeight: number;
    public loading: boolean;

    constructor(
        private page: Page,
        private route: PageRoute,
        private router: RouterExtensions,
        private backend: BackendService,
        private store: Store<AppState>,
        private fonticon: TNSFontIconService
    ) {
        this.page.backgroundSpanUnderStatusBar = true;
        this.statusBarHeight = this.getStatusBarHeight();

        // use switchMap to get the latest activatedRoute instance
        this.route.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.paramMenuId = +params['menuId'];
                this.paramMenuSize = params['menuSize'];
                console.log('CartMenuSizeComponent.ctor', this.paramMenuId);
            });
    }

    ngOnInit() {
        console.log('CartMenuSizeComponent.ngOnInit');

        this.loading = true;
        this.backend.getAddons(this.paramMenuId).subscribe(
            (data) => {
                let menu: Menu;
                data.Menu.forEach((item) => {
                    menu = {
                        menuId: +item.id,
                        resId: +item.restaurant_id,
                        catId: +item.menu_category,
                        menuType: item.menu_type,
                        menuName: item.menu_name,
                        menuDescription: item.menu_description,
                        menuPrice: parseFloat(item.menu_price),
                        menuSize: item.sizeoption,
                        menuSizes: item.sizeoption === 'size' ?
                            [
                                {
                                    id: 'small',
                                    name: 'Almindelig',
                                    price: parseFloat(item.pizza_small_value),
                                    checked: false
                                },
                                {
                                    id: 'medium',
                                    name: 'Deep Pan',
                                    price: parseFloat(item.pizza_medium_value),
                                    checked: false
                                },
                                {
                                    id: 'large',
                                    name: 'Familie',
                                    price: parseFloat(item.pizza_large_value),
                                    checked: false
                                }
                            ].filter((size) => size.price > 0) : item.Slice.map((slice) => {
                                return {
                                    id: +slice.pizza_slice_id,
                                    name: slice.pizza_slice_name,
                                    price: parseFloat(slice.pizza_slice_price),
                                    checked: false
                                }
                            }),
                        mainAddons: item.MainAddon.map((mainAddon) => {
                            return {
                                id: +mainAddon.menuaddons_id,
                                name: mainAddon.mainaddonsname,
                                addonsCount: +mainAddon.mainaddonsnamecnt,
                                subAddons: mainAddon.SubAddon.map((subAddon) => {
                                    return {
                                        id: +subAddon.menuaddons_id,
                                        name: subAddon.subaddonsname,
                                        price: parseFloat(subAddon.menuaddons_price),
                                        checked: false
                                    }
                                })
                            }
                        }).sort((a: MainAddon, b: MainAddon) => b.addonsCount - a.addonsCount)
                    }
                })
                // items = items.sort((a: MainAddon, b: MainAddon) => a.multiChoice === b.multiChoice ? 0 : a.multiChoice ? 1 : -1)
                this.menu$.next(menu);

                let actionBar = this.page.getViewById<ActionBar>('actionBar');
                actionBar.title = menu.menuName;
            },
            (error) => console.error(error),
            () => { this.loading = false; }
        );
    }

    onMenuSizeSelected(args: ItemEventData) {
        let menu = this.menu$.getValue();
        if (menu === null)
            return;

        let menuSize = menu.menuSizes[args.index];

        // clear selection
        menu.menuSizes.forEach((menuSize) => {
            menuSize.checked = false;
        })

        // check selected
        menuSize.checked = true;

        // Update observables
        this.menu$.next(menu);

        // Update ui
        (<ListView>args.view.parent).refresh();

        // Change state


        console.log('CartMenuSizeComponent.onMenuSizeSelected', args.index);
    }

    onContinueTap(args: ItemEventData) {
        console.log('CartMenuSizeComponent.onContinueTap');

        let menu = this.menu$.getValue();
        if (menu === null)
            return;

        let selectedSize = menu.menuSizes.filter((menuSize) => {
            return menuSize.checked;
        })

        if (selectedSize.length)
            this.router.navigate(['/cart/add', this.paramMenuId, 'addon'], {
                queryParams: {
                    'menuSize': selectedSize[0].id
                }
            });
    }

    private getStatusBarHeight() {
        if (application.android) {
            return 20;
        } else {
            return 0;
        }
    }
}