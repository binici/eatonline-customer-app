import { ModuleWithProviders } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { CartMenuAddonComponent } from './cart-menu-addon/cart-menu-addon.component';
import { CartMenuSizeComponent } from './cart-menu-size/cart-menu-size.component';
import { CartConfirmComponent } from './cart-confirm/cart-confirm.component';
import { CartViewComponent } from './cart-view/cart-view.component';

const cartRoutes: Routes = [
    {
        path: 'cart',
        children: [
            {
                path: 'add',
                children: [
                    {
                        path: ':menuId',
                        children: [
                            { path: 'size', component: CartMenuSizeComponent },
                            { path: 'addon', component: CartMenuAddonComponent },
                            { path: 'confirm', component: CartConfirmComponent }
                        ]
                    }
                ]
            },
            {
                path: 'view',
                component: CartViewComponent
            }
        ]
    }
];
export const cartRouting: ModuleWithProviders = RouterModule.forChild(cartRoutes);