import { Component, OnInit } from '@angular/core';
import { PageRoute, RouterExtensions } from "nativescript-angular/router";
import { Location } from '@angular/common';
import { Page } from "ui/page";
import { ActionBar } from "ui/action-bar";
import { ItemEventData } from "ui/list-view";
import { Store } from "@ngrx/store";
import * as application from "application";
import * as Rx from 'rxjs';
import * as _ from 'lodash';

import { BackendService, AppState, CartActions, Menu, MainAddon, SubAddon, MenuSize, CartItem, UtilService } from '../../../shared';
import { TextView } from "ui/text-view";

import frameModule = require("ui/frame");

@Component({
    moduleId: module.id,
    selector: 'cart-confirm',
    templateUrl: 'cart-confirm.component.html',
    styleUrls: ['./cart-confirm.component.css'],
})
export class CartConfirmComponent implements OnInit {
    public paramMenuId: number;
    public paramMenuSize: string = '';
    public paramSelectedMenuAddons: number[];

    public summary: string;
    public statusBarHeight: number;
    public loading: boolean = true;

    public menu$: Rx.BehaviorSubject<Menu> = new Rx.BehaviorSubject<Menu>(null);

    public qty$: Rx.BehaviorSubject<number> = new Rx.BehaviorSubject<number>(1);

    public mainAddons$: Rx.Observable<MainAddon[]> = this.menu$.map((menu: Menu) => {
        return menu ? menu.mainAddons : [];
    });

    public menuSize$ = this.menu$.map((menu) => {
        return menu ? menu.menuSize : '';
    })

    public menuSizes$ = this.menu$.map((menu) => {
        return menu ? menu.menuSizes : [];
    })

    public menuPrice$ = Rx.Observable.combineLatest(this.menu$, this.menuSizes$,
        (menu, menuSizes) => {
            return this.calculateMenuPrice(menu, menuSizes);
        }
    );

    public totalPrice$ = Rx.Observable.combineLatest(this.menu$, this.menuPrice$, this.qty$,
        (menu: Menu, menuPrice: number, qty: number) => {
            return this.calculateTotalPrice(menu, menuPrice, qty);
        }
    )

    constructor(
        private page: Page,
        private route: PageRoute,
        private router: RouterExtensions,
        private location: Location,
        private backend: BackendService,
        private store: Store<AppState>
    ) {
        this.page.backgroundSpanUnderStatusBar = true;
        this.statusBarHeight = this.getStatusBarHeight();

        // use switchMap to get the latest activatedRoute instance
        this.route.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.paramMenuId = +params['menuId'];
                console.log('CartConfirmComponent.ctor', this.paramMenuId);
            });

        this.route.activatedRoute
            .switchMap(activatedRoute => activatedRoute.queryParams)
            .forEach((queryParams) => {
                this.paramMenuSize = queryParams['menuSize'];
                this.paramSelectedMenuAddons = queryParams['menuAddons'].split(',').map((n) => +n).filter((n) => n > 0);
                console.log('CartConfirmComponent.ctor', this.paramMenuSize, this.paramSelectedMenuAddons);
            })
    }

    ngOnInit() {
        console.log('CartConfirmComponent.ngOnInit');

        this.loading = true;
        this.backend.getAddons(this.paramMenuId).subscribe(
            (data) => {
                let menu: Menu;
                data.Menu.forEach((item) => {
                    menu = {
                        menuId: +item.id,
                        resId: +item.restaurant_id,
                        catId: +item.menu_category,
                        menuType: item.menu_type,
                        menuName: item.menu_name.trim(),
                        menuDescription: item.menu_description,
                        menuPrice: parseFloat(item.menu_price),
                        menuSize: item.sizeoption,
                        menuSizes: item.sizeoption === 'size' ?
                            [
                                {
                                    id: 'small',
                                    name: 'Almindelig',
                                    price: parseFloat(item.pizza_small_value),
                                    checked: this.paramMenuSize === 'small'
                                },
                                {
                                    id: 'medium',
                                    name: 'Deep Pan',
                                    price: parseFloat(item.pizza_medium_value),
                                    checked: this.paramMenuSize === 'medium'
                                },
                                {
                                    id: 'large',
                                    name: 'Familie',
                                    price: parseFloat(item.pizza_large_value),
                                    checked: this.paramMenuSize === 'large'
                                }
                            ] : item.Slice.map((slice) => {
                                return {
                                    id: +slice.pizza_slice_id,
                                    name: slice.pizza_slice_name,
                                    price: parseFloat(slice.pizza_slice_price),
                                    checked: this.paramMenuSize === slice.pizza_slice_id
                                }
                            }),
                        mainAddons: item.MainAddon.map((mainAddon) => {
                            return {
                                id: +mainAddon.menuaddons_id,
                                name: mainAddon.mainaddonsname.trim(),
                                addonsCount: +mainAddon.mainaddonsnamecnt,
                                subAddons: mainAddon.SubAddon.map((subAddon) => {
                                    return {
                                        id: +subAddon.menuaddons_id,
                                        name: subAddon.subaddonsname.trim(),
                                        price: parseFloat(subAddon.menuaddons_price),
                                        checked: this.paramSelectedMenuAddons.some((m) => m === +subAddon.menuaddons_id)
                                    }
                                })
                            }
                        }).sort((a: MainAddon, b: MainAddon) => b.addonsCount - a.addonsCount)
                    }
                })
                this.menu$.next(menu);

                let addonNames = this.generateAddonName(menu);
                this.summary = `${addonNames}`;

                let actionBar = this.page.getViewById<ActionBar>('actionBar');
                actionBar.title = data.Menu[0].menu_name;
            },
            (error) => console.error(error),
            () => { this.loading = false; }
        );
    }

    onQtyMinus() {
        let newQty = Math.max(1, this.qty$.getValue() - 1);
        this.qty$.next(newQty);
    }

    onQtyPlus() {
        let newQty = Math.min(100, this.qty$.getValue() + 1);
        this.qty$.next(newQty);
    }

    addToCart(args: ItemEventData) {
        let items: Array<CartItem> = new Array<CartItem>();
        let menu = this.menu$.getValue();
        let qty = this.qty$.getValue();
        let selectedAddonsName = this.generateAddonName(menu);
        let selectedAddonsPrice = this.calculateAddonPrice(menu);
        let menuPrice = this.calculateMenuPrice(menu, menu.menuSizes);
        let totalPrice = this.calculateTotalPrice(menu, menuPrice, qty);
        let tvComments = this.page.getViewById<TextView>('tvComments');

        let item = new CartItem();
        item.resId = menu.resId;
        item.catId = menu.catId;
        item.menuId = menu.menuId;
        item.menuName = menu.menuName;
        item.menuType = menu.menuType;
        item.menuSize = menu.menuSize;
        item.menuPrice = menu.menuPrice;
        item.addonName = selectedAddonsName;
        item.addonPrice = selectedAddonsPrice;
        item.qty = qty;
        item.totalPrice = totalPrice;
        item.instruction = tvComments.text;

        // Dispatch store action
        this.store.dispatch(CartActions.addCartItem(item));

        // Navigate back to restaurant page
        let restaurantEntry = this.router.frame.backStack[2];

        if (!this.router.locationStrategy._isPageNavigatingBack) {
            this.router.locationStrategy._beginBackPageNavigation();
        }

        this.router.frame.goBack(restaurantEntry);
    }

    private generateAddonName(menu: Menu): string {
        if (menu == null)
            return '';

        return _.flatten(menu.mainAddons.map((mainAddon) => {
            return mainAddon.subAddons.filter((subAddon) => {
                return subAddon.checked;
            })
        })).reduce((prev, cur) => {
            let name = (prev === '' ? cur.name : prev + ', ' + cur.name);

            return cur.price > 0 ? `${name} (${UtilService.formatCurrency(cur.price)})` : name;
        }, '');
    }

    private calculateAddonPrice(menu: Menu): number {
        if (menu == null)
            return 0;

        return menu.mainAddons.map((mainAddon) => {
            return mainAddon.subAddons.filter((subAddon) => subAddon.checked)
        }).map((subAddons) => {
            return _.sumBy(subAddons, (subAddon: SubAddon) => subAddon.price);
        }).reduce((prev, cur) => prev + cur, 0);
    }

    private calculateMenuPrice(menu: Menu, menuSizes: MenuSize[]): number {
        if (menu == null)
            return 0;

        switch (menu.menuSize) {
            case 'fixed':
                return menu.menuPrice;
            case 'size':
            case 'slice':
                return menuSizes.reduce((prev, cur) => {
                    return prev + (cur.checked ? cur.price : 0);
                }, 0);
        }
    }

    private calculateTotalPrice(menu: Menu, menuPrice: number, qty: number): number {
        if (menu == null)
            return 0;

        return (menuPrice + menu.mainAddons.map((mainAddon) => {
            return mainAddon.subAddons.filter((subAddon) => subAddon.checked)
        }).map((subAddons) => {
            return _.sumBy(subAddons, (subAddon: SubAddon) => subAddon.price);
        }).reduce((prev, cur) => prev + cur, 0)) * qty;
    }

    // private generateSubAddonNames(menu: Menu): string {
    //     return _.flatten(menu.mainAddons.filter((mainAddon) => mainAddon.addonsCount === 0).map((mainAddon) => {
    //         return mainAddon.subAddons.filter((subAddon) => {
    //             return subAddon.checked;
    //         }).map((subAddon) => {
    //             return subAddon.name.trim();
    //         })
    //     })).reduce((prev, cur) => {
    //         return prev === '' ? cur : prev + ', ' + cur;
    //     }, '');
    // }

    private getStatusBarHeight() {
        if (application.android) {
            return 20;
        } else {
            return 0;
        }
    }
}