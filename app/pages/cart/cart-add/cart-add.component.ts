import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/directives/dialogs";
import { ListView, ItemEventData } from 'ui/list-view';
import { Page } from "ui/page";
import { TextView } from "ui/text-view";
import { Store } from "@ngrx/store";
import { SlidesComponent } from 'nativescript-ng2-slides/slides/app/slides/slides.component';
import * as Rx from 'rxjs';
import * as _ from 'lodash';

import { BackendService, AppState, Menu, MainAddon, SubAddon, MenuSize, CartItem, CartActions, UtilService } from '../../../shared';

@Component({
    moduleId: module.id,
    selector: "cart-add",
    templateUrl: "cart-add.component.html",
    styleUrls: ['./cart-add.component.css']
})
export class CartAddModal implements OnInit {

    // Parameters
    public paramMenuId: number;
    public paramMenuSize: string = '';

    // Observables
    public menu$: Rx.BehaviorSubject<Menu> = new Rx.BehaviorSubject<Menu>(null);

    public qty$: Rx.BehaviorSubject<number> = new Rx.BehaviorSubject<number>(1);

    public mainAddons$: Rx.Observable<MainAddon[]> = this.menu$.map((menu: Menu) => {
        return menu ? menu.mainAddons : [];
    });

    public menuSize$ = this.menu$.map((menu) => {
        return menu ? menu.menuSize : '';
    })

    public menuSizes$ = this.menu$.map((menu) => {
        return menu ? menu.menuSizes : [];
    })

    public menuState$ = Rx.Observable.combineLatest(this.menuSize$, this.menuSizes$,
        (menuSize, menuSizes) => {
            let hasSelectedMenuSize = menuSizes.some((menuSize) => {
                return menuSize.checked;
            })

            if (menuSize === 'size') {
                return hasSelectedMenuSize ? 'selectAddon' : 'selectSize'
            } else {
                return 'selectAddon';
            }
        }
    )

    public menuPrice$ = Rx.Observable.combineLatest(this.menu$, this.menuSizes$,
        (menu, menuSizes) => {
            return this.calculateMenuPrice(menu, menuSizes);
        }
    );

    public totalPrice$ = Rx.Observable.combineLatest(this.menu$, this.menuPrice$, this.qty$,
        (menu: Menu, menuPrice: number, qty: number) => {
            return this.calculateTotalPrice(menu, menuPrice, qty);
        }
    )

    public hasSelectedMenuSize$ = this.menuSizes$.map((menuSizes) => {
        return menuSizes.some((menuSize) => {
            return menuSize.checked;
        })
    })

    public hasAddons$ = this.mainAddons$.map((menuAddons) => {
        return menuAddons.length > 0;
    })

    public menuSizeValid$ = this.hasSelectedMenuSize$.map((hasSelectedMenuSize) => {
        return hasSelectedMenuSize;
    });

    public menuAddonsValid$ = this.mainAddons$.map((mainAddons) => {
        return mainAddons.map((mainAddon) => {
            return mainAddon.addonsCount === 0 || mainAddon.addonsCount === mainAddon.subAddons.reduce<number>((prev, cur) => {
                return prev + (cur.checked ? 1 : 0);
            }, 0)
        }).reduce((prev, cur) => {
            return prev && cur;
        }, true);
    });

    // Other public variables
    public state: string;
    public statusBarHeight: number;
    public listViewHeight: number;
    public loading: boolean;

    public constructor(
        private page: Page,
        private params: ModalDialogParams,
        private backend: BackendService,
        private store: Store<AppState>
    ) {
        console.log('CartAddComponent.ctor');

        this.paramMenuId = +params.context.menuId;
        this.paramMenuSize = params.context.menuSize;

        this.page.marginTop = 0;
    }

    public close() {
        this.params.closeCallback();
    }

    ngOnInit() {
        console.log('CartAddModal.ngOnInit');

        this.loading = true;
        this.backend.getAddons(this.paramMenuId).subscribe(
            (data) => {
                let menu: Menu;
                data.Menu.forEach((item) => {
                    menu = {
                        menuId: +item.id,
                        resId: +item.restaurant_id,
                        catId: +item.menu_category,
                        menuType: item.menu_type,
                        menuName: item.menu_name.trim(),
                        menuDescription: item.menu_description,
                        menuPrice: parseFloat(item.menu_price),
                        menuSize: item.sizeoption,
                        menuSizes: item.sizeoption === 'size' ?
                            [
                                {
                                    id: 'small',
                                    name: 'Almindelig',
                                    price: parseFloat(item.pizza_small_value),
                                    checked: this.paramMenuSize === 'small'
                                },
                                {
                                    id: 'medium',
                                    name: 'Deep Pan',
                                    price: parseFloat(item.pizza_medium_value),
                                    checked: this.paramMenuSize === 'medium'
                                },
                                {
                                    id: 'large',
                                    name: 'Familie',
                                    price: parseFloat(item.pizza_large_value),
                                    checked: this.paramMenuSize === 'large'
                                }
                            ] : item.Slice.map((slice) => {
                                return {
                                    id: +slice.pizza_slice_id,
                                    name: slice.pizza_slice_name,
                                    price: parseFloat(slice.pizza_slice_price),
                                    checked: this.paramMenuSize === slice.pizza_slice_id
                                }
                            }),
                        mainAddons: item.MainAddon.map((mainAddon) => {
                            return {
                                id: +mainAddon.menuaddons_id,
                                name: mainAddon.mainaddonsname.trim(),
                                addonsCount: +mainAddon.mainaddonsnamecnt,
                                subAddons: mainAddon.SubAddon.map((subAddon) => {
                                    return {
                                        id: +subAddon.menuaddons_id,
                                        name: subAddon.subaddonsname.trim(),
                                        price: parseFloat(subAddon.menuaddons_price),
                                        checked: false
                                    }
                                })
                            }
                        }).sort((a: MainAddon, b: MainAddon) => b.addonsCount - a.addonsCount)
                    }
                })
                // items = items.sort((a: MainAddon, b: MainAddon) => a.multiChoice === b.multiChoice ? 0 : a.multiChoice ? 1 : -1)
                this.menu$.next(menu);

                // let actionBar = this.page.getViewById<ActionBar>('actionBar');
                // actionBar.title = data.Menu[0].menu_name;
            },
            (error) => console.error(error),
            () => { this.loading = false; }
        );
    }

    mainAddonHeader(mainAddon: MainAddon) {
        console.log('CartAddModal.mainAddonHeader');

        let prefix = `${mainAddon.name} `;
        switch (mainAddon.addonsCount) {
            case 0:
                return `${prefix} (valgfrit)`;
            case 1:
                return `${prefix} (påkrævet)`;
            default:
                return `${prefix} (${mainAddon.addonsCount} valg)`;
        }
    }

    onMenuSizeSelected(args: ItemEventData) {
        let menu = this.menu$.getValue();
        if (menu === null)
            return;

        let menuSize = menu.menuSizes[args.index];

        // clear selection
        menu.menuSizes.forEach((menuSize) => {
            menuSize.checked = false;
        })

        // check selected
        menuSize.checked = true;

        // Update observables
        this.menu$.next(menu);

        // Update ui
        (<ListView>args.view.parent).refresh();

        // Change state


        console.log('CartMenuSizeComponent.onMenuSizeSelected', args.index);
    }

    onSubAddonTapped(args: ItemEventData, mainAddonIndex: number) {
        let menu = this.menu$.getValue();
        if (menu === null)
            return;

        let mainAddon = menu.mainAddons[mainAddonIndex];
        let subAddon = mainAddon.subAddons[args.index];
        let noChecked = mainAddon.subAddons.reduce<number>((prev, cur) => {
            return prev + (cur.checked ? 1 : 0);
        }, 0);

        if (mainAddon.addonsCount === 0) {
            // non-mandatory addon
            subAddon.checked = !subAddon.checked;
        } else if (mainAddon.addonsCount === 1) {
            // clear checked, if addons-count equals 1
            mainAddon.subAddons.forEach((subAddon) => {
                subAddon.checked = false;
            })
            subAddon.checked = true;
        } else {
            if (noChecked === mainAddon.addonsCount) {
                subAddon.checked = false;
            } else {
                subAddon.checked = true;
            }
        }

        // Update observables
        this.menu$.next(menu);

        // Update ui
        (<ListView>args.view.parent).refresh();

        console.log('onSubAddonTapped', mainAddonIndex, args.index, subAddon.checked);
    }

    onContinueTap(args: ItemEventData, slides: SlidesComponent) {
        console.log('CartMenuSizeComponent.onContinueTap');

        // const slides = <Page>this.page.getViewById('mySlides');
        slides.nextSlide();

        // let menu = this.menu$.getValue();
        // if (menu === null)
        //     return;

        // let selectedSize = menu.menuSizes.filter((menuSize) => {
        //     return menuSize.checked;
        // })

        // if (selectedSize.length)
        //     this.router.navigate(['/cart/add', this.paramMenuId, 'addon'], {
        //         queryParams: {
        //             'menuSize': selectedSize[0].id
        //         }
        //     });
    }

    onQtyMinus() {
        let newQty = Math.max(1, this.qty$.getValue() - 1);
        this.qty$.next(newQty);
    }

    onQtyPlus() {
        let newQty = Math.min(100, this.qty$.getValue() + 1);
        this.qty$.next(newQty);
    }

    addToCart(args: ItemEventData) {
        let items: Array<CartItem> = new Array<CartItem>();
        let menu = this.menu$.getValue();
        let qty = this.qty$.getValue();
        let selectedAddonsName = this.generateAddonName(menu);
        let selectedAddonsPrice = this.calculateAddonPrice(menu);
        let menuPrice = this.calculateMenuPrice(menu, menu.menuSizes);
        let totalPrice = this.calculateTotalPrice(menu, menuPrice, qty);
        let tvComments = this.page.getViewById<TextView>('tvComments');

        let item = new CartItem();
        item.resId = menu.resId;
        item.catId = menu.catId;
        item.menuId = menu.menuId;
        item.menuName = menu.menuName;
        item.menuType = menu.menuType;
        item.menuSize = menu.menuSize;
        item.menuPrice = menu.menuPrice;
        item.addonName = selectedAddonsName;
        item.addonPrice = selectedAddonsPrice;
        item.qty = qty;
        item.totalPrice = totalPrice;
        item.instruction = tvComments.text;

        // Dispatch store action
        this.store.dispatch(CartActions.addCartItem(item));

        // Close dialog
        this.close();
    }

    private generateAddonName(menu: Menu): string {
        if (menu == null)
            return '';

        return _.flatten(menu.mainAddons.map((mainAddon) => {
            return mainAddon.subAddons.filter((subAddon) => {
                return subAddon.checked;
            })
        })).reduce((prev, cur) => {
            let name = (prev === '' ? cur.name : prev + ', ' + cur.name);

            return cur.price > 0 ? `${name} (${UtilService.formatCurrency(cur.price)})` : name;
        }, '');
    }

    private calculateAddonPrice(menu: Menu): number {
        if (menu == null)
            return 0;

        return menu.mainAddons.map((mainAddon) => {
            return mainAddon.subAddons.filter((subAddon) => subAddon.checked)
        }).map((subAddons) => {
            return _.sumBy(subAddons, (subAddon: SubAddon) => subAddon.price);
        }).reduce((prev, cur) => prev + cur, 0);
    }

    private calculateMenuPrice(menu: Menu, menuSizes: MenuSize[]): number {
        if (menu == null)
            return 0;

        switch (menu.menuSize) {
            case 'fixed':
                return menu.menuPrice;
            case 'size':
            case 'slice':
                return menuSizes.reduce((prev, cur) => {
                    return prev + (cur.checked ? cur.price : 0);
                }, 0);
        }
    }

    private calculateTotalPrice(menu: Menu, menuPrice: number, qty: number): number {
        if (menu == null)
            return 0;

        return (menuPrice + menu.mainAddons.map((mainAddon) => {
            return mainAddon.subAddons.filter((subAddon) => subAddon.checked)
        }).map((subAddons) => {
            return _.sumBy(subAddons, (subAddon: SubAddon) => subAddon.price);
        }).reduce((prev, cur) => prev + cur, 0)) * qty;
    }

}