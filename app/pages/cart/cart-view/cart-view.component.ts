import { Component, OnInit } from '@angular/core';
import { Page } from "ui/page";
import { Store } from "@ngrx/store";
import { Observable, EventData } from "data/observable";
import { DropDown, SelectedIndexChangedEventData } from "nativescript-drop-down";
import { SegmentedBarItem } from "ui/segmented-bar";
import * as application from "application";
import * as Rx from 'rxjs';

import { BackendService, AppState, CartItem, DeliveryZipcode, Restaurant } from '../../../shared';

@Component({
    moduleId: module.id,
    selector: 'cart-view',
    templateUrl: 'cart-view.component.html',
    styleUrls: ['./cart-view.component.css'],
})
export class CartViewComponent implements OnInit {
    public restaurant$: Rx.BehaviorSubject<Restaurant> = new Rx.BehaviorSubject<Restaurant>(null);

    public cart$: Rx.BehaviorSubject<CartItem[]> = new Rx.BehaviorSubject<CartItem[]>([]);

    public selectedDeliveryOption$: Rx.BehaviorSubject<string> = new Rx.BehaviorSubject<string>('');

    public selectedDeliveryZipcode$: Rx.BehaviorSubject<DeliveryZipcode> = new Rx.BehaviorSubject<DeliveryZipcode>(null);

    public subTotal$ = this.cart$.map((cart) => {
        return cart.reduce((prev, cur) => {
            return prev + cur.totalPrice;
        }, 0);
    });

    public deliveryAmount$ = Rx.Observable.combineLatest(this.selectedDeliveryOption$, this.selectedDeliveryZipcode$,
        (selectedDeliveryOption, selectedDeliveryZipcode) => {
            if (selectedDeliveryOption === 'pickup') {
                return 0;
            } else if (selectedDeliveryOption === 'delivery') {
                if (selectedDeliveryZipcode) {
                    let zipcode = selectedDeliveryZipcode;
                    return zipcode.restaurant_delivery_charge;
                } else {
                    return 0;
                }
            } else {
                return 0;
            }
        })
    
    public grandTotal$ = Rx.Observable.combineLatest(this.subTotal$, this.deliveryAmount$,
        (subtotal, delivery) => {
            return subtotal + delivery;
        }
    )

    public deliveryAvailable: boolean = false;
    public zipCodes: Array<string> = [''];
    public deliveryOptions: Array<SegmentedBarItem> = [];

    public statusBarHeight: number;
    public listViewHeight: number;
    public loading: boolean;

    private deliveryZipcodes: {
        [zipcode: string]: DeliveryZipcode
    } = {};

    constructor(
        private page: Page,
        private store: Store<AppState>,
        private backend: BackendService
    ) {
        this.page.backgroundSpanUnderStatusBar = true;
        this.statusBarHeight = this.getStatusBarHeight();
    }

    ngOnInit() {
        console.log('CartViewComponent.ngOnInit');

        this.store.select('cart').subscribe((cart: CartItem[]) => {
            this.cart$.next(cart);
            // this.cart = cart;
            // this.cartTotal = this.cart.reduce((prev, cur) => {
            //     return prev + cur.totalPrice;
            // }, 0);
        });

        this.store.select('restaurant').subscribe((res: Restaurant) => {
            console.log('CartViewComponent.ngOnInit1', res === null);
            console.log('CartViewComponent.ngOnInit1', res.restaurant_id);

            // Push restaurant to observable
            this.restaurant$.next(res);

            let sbItem1: SegmentedBarItem = <SegmentedBarItem>new SegmentedBarItem();
            sbItem1.title = 'Levering';
            sbItem1.set('value', 'delivery');

            let sbItem2: SegmentedBarItem = <SegmentedBarItem>new SegmentedBarItem();
            sbItem2.title = 'Afhentning';
            sbItem2.set('value', 'pickup');

            if (res.restaurant_delivery === 'Yes') {
                this.deliveryOptions.push(sbItem1);
                this.deliveryAvailable = true;
            }

            if (res.restaurant_pickup === 'Yes') {
                this.deliveryOptions.push(sbItem2);
            }

            // this.zipCodes = new Array<string>();
            this.backend.getDeliveryZipcodes(res.restaurant_id).subscribe((deliveryZipcodes) => {
                if (deliveryZipcodes) {
                    deliveryZipcodes.zipcode.forEach(delivery => {
                        console.log('CartViewComponent.ngOninit: ', delivery.zipcode);
                        this.zipCodes.push(delivery.zipcode);
                        this.deliveryZipcodes[delivery.zipcode] = {
                            restaurant_delivery_charge: +delivery.restaurant_delivery_charge,
                            res_deli_opentime: delivery.res_deli_opentime,
                            res_deli_closetime: delivery.res_deli_closetime,
                            res_deli_minimumorder: +delivery.res_deli_minimumorder,
                            zipcode: delivery.zipcode,
                            cityname: delivery.cityname
                        }
                    });
                }
            })
        })

        // this.zipCodes.push('1');
        // this.zipCodes.push('2');
        // this.zipCodes.push('3');
    }

    generateGridRowExp() {
        // return this.cart.map((item) => {
        //     return 'auto';
        // }).join(',');
    }

    onDeliveryOptionsChange(selectedIndex: number) {
        let selected = this.deliveryOptions[selectedIndex];
        console.log('CartViewComponent.onDeliveryOptionsChange', selectedIndex, selected.title, selected.get('value'));

        if (selected.get('value') === 'delivery') {
            this.deliveryAvailable = true;
            this.selectedDeliveryOption$.next('delivery');
        }

        if (selected.get('value') === 'pickup') {
            this.deliveryAvailable = false;
            this.selectedDeliveryOption$.next('pickup');
        }
    }

    onZipcodeChange(args: SelectedIndexChangedEventData) {
        console.log('CartViewComponent.onZipcodeChange', args.newIndex);
        let zipcode = this.zipCodes[args.newIndex];
        let deliveryZipcode = this.deliveryZipcodes[zipcode];

        this.selectedDeliveryZipcode$.next(deliveryZipcode);
    }

    private getStatusBarHeight() {
        if (application.android) {
            return 20;
        } else {
            return 0;
        }
    }
}