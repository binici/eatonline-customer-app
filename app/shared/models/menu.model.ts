export class Menu {
    menuId: number;
    resId: number;
    catId: number;
    menuName: string;
    menuDescription: string;
    menuType: string;
    menuSize: string;
    menuSizes: MenuSize[];
    menuPrice: number;
    mainAddons: MainAddon[];
}

export class MainAddon {
    id: number;
    name: string;
    addonsCount: number;
    subAddons: SubAddon[];
}

export class SubAddon {
    id: number;
    name: string;
    price: number;
    checked: boolean;
}

export class MenuSize {
    id: string;
    name: string;
    price: number;
    checked: boolean;
}