export class Cart {
    public items: CartItem[] = [];
}

export class CartItem {
    public id: number;
    public resId: number;
    public catId: number;
    public menuId: number;
    public menuName: string;
    public menuType: string;
    public menuSize: string;
    public menuPrice: number;
    public addonName: string;
    public addonPrice: number;
    public subAddon: number;
    public qty: number;
    public totalPrice: number;
    public instruction: string;
}
