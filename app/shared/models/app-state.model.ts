import { CartItem } from './cart.model';
import { Restaurant } from "./restaurant.model";

export interface AppState {
    cart: CartItem[];
    restaurant: Restaurant;
}