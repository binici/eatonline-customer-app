export class Restaurant_ListItem {
    restaurant_id: number;
    restaurant_name: string;
    restaurant_logo: string;
    restaurant_serving_cuisines: string;
    offer_percentage: number;
    total_rating: number;
};

export class Restaurant extends Restaurant_ListItem {
    restaurant_delivery: string;
    restaurant_pickup: string;
    restaurant_minpickup_price: number;
    restaurant_delivery_charge: number;
    restaurant_minorder_price: number;
};