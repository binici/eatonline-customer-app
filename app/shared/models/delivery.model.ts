export class DeliveryZipcode {
    // restaurant_delivery_time: string;
    restaurant_delivery_charge: number;
    res_deli_opentime: string;
    res_deli_closetime: string;
    res_deli_minimumorder: number;
    // res_deli_more_order_amt: number;
    // res_more_order_delicharge: number;
    // opentime: number;
    // closetime: number;
    zipcode: number;
    // cityid: number;
    cityname: string;
}