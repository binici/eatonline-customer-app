/**
 * combineReducers is another useful metareducer that takes a map of reducer
 * functions and creates a new reducer that gathers the values
 * of each reducer and stores them using the reducer's key. Think of it
 * almost like a database, where every reducer is a table in the db.
 *
 * More: https://egghead.io/lessons/javascript-redux-implementing-combinereducers-from-scratch
 */
import { combineReducers } from '@ngrx/store';

import * as fromCart from './cart.reducer';
import * as fromRestaurant from './restaurant.reducer';

export const reducers = {
    cart: fromCart.reducer,
    restaurant: fromRestaurant.reducer
};

// export function reducer(state: any, action: any) {
//     return combineReducers(reducers);
// }