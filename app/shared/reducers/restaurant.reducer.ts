import { ActionReducer, Action } from '@ngrx/store';
import { RestaurantActions } from '../actions/restaurant.action';
import { Restaurant } from '../models/restaurant.model';
const objectAssign = require('object-assign');

export function reducer(state: Restaurant = null, action: Action) {
    switch (action.type) {
        case RestaurantActions.SET_RESTAURANT:
            return objectAssign({}, action.payload);
        default:
            return state;
    }
}