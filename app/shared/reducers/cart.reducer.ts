import { ActionReducer, Action } from '@ngrx/store';
import { CartActions } from '../actions/cart.action';
import { CartItem } from '../models/cart.model';

export function reducer(state: CartItem[] = [], action: Action) {
    switch (action.type) {
        case CartActions.ADD_CARTITEM:
            return [...state, action.payload]
        default:
            return state;
    }
}