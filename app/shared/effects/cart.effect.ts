import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Actions, Effect, toPayload } from '@ngrx/effects';
import { Observable } from 'rxjs/Observable';
import { CartService } from '../services/cart.service';
import { CartActions } from '../actions/cart.action';

@Injectable()
export class CartEffects {

    constructor(private cartService: CartService, private actions$: Actions) { }

    @Effect({ dispatch: false }) cartAddItem$ = this.actions$
        // Listen for the 'ADD_CARTITEM' action
        .ofType(CartActions.ADD_CARTITEM)
        .map(toPayload)
        .switchMap(payload => {
            return Observable.fromPromise(
                this.cartService.insert(payload).then(() => {
                    return Observable.of({});
                })
            );
        }
        );

}