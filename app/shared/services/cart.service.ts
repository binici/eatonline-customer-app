import { Injectable } from '@angular/core';
import { Cart, CartItem } from '../index';
var Sqlite = require("nativescript-sqlite");

@Injectable()
export class CartService {

    private database: any;

    constructor() {
        console.log('CartService.ctor');
        if (Sqlite.exists("eatonline.db")) {
            Sqlite.deleteDatabase("eatonline.db");
        }
        (new Sqlite("eatonline.db")).then(db => {
            db.execSQL(`
            CREATE TABLE cart (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                resId INTEGER NOT NULL,
                catId INTEGER NOT NULL,
                menuId INTEGER NOT NULL,
                menuName TEXT NOT NULL,
                menuType TEXT,
                menuSize TEXT,
                menuPrice REAL NOT NULL,
                addonName TEXT,
                addonPrice REAL,
                subAddon INTEGER,
                qty INTEGER NOT NULL,
                totalPrice REAL,
                instruction TEXT );
            `
            ).then(id => {
                this.database = db;
                console.log('SQLITE DB OPENED');
            }, error => {
                console.log("CREATE TABLE ERROR", error);
            });
        }, error => {
            console.log("OPEN DB ERROR", error);
        });
    }

    public insert(item: CartItem): Promise<any> {
        console.log('SQLITE: insert into cart')
        return this.database.execSQL(
            `
             INSERT INTO cart (resId, catId, menuId, menuName, menuType, menuSize, menuPrice, addonName, addonPrice, subAddon, qty, totalPrice, instruction)
             VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            `,
            [item.resId, item.catId, item.menuId, item.menuName, item.menuType, item.menuSize, item.menuPrice, item.addonName, item.addonPrice, item.subAddon, item.qty, item.totalPrice, item.instruction]
        );
    }
}
