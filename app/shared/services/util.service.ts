import { Injectable } from '@angular/core';
var intl = require("nativescript-intl");

@Injectable()
export class UtilService {
    private static currencyFormat: any = new intl.NumberFormat('da-DK', {'style': 'currency', 'currency': 'DKK', 'currencyDisplay': 'code'});

    constructor() { }

    public static formatCurrency(number) {
        return this.currencyFormat.format(number);
    }
}