import { Injectable } from '@angular/core';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Rx";
import * as queryString from 'query-string';

import "rxjs/add/operator/map";
import "rxjs/add/operator/do";

const RESTAURANT_URL = 'https://www.eatonline.dk/customerApp/restaurant.php';

@Injectable()
export class BackendService {

    constructor(private http: Http) { }

    getRestaurantsByZipcode(zipCode: string) {
        let params = {
            action: 'searchRestaurant',
            cityzip: zipCode
        };
        let query = queryString.stringify(params);

        return this.http.get(`${RESTAURANT_URL}?${query}`, {}).map(res => res.json());
    }

    getRestaurant(resId: number) {
        let params = {
            action: 'restaurantDetails',
            resid: resId
        };
        let query = queryString.stringify(params);

        return this.http.get(`${RESTAURANT_URL}?${query}`, {}).map(res => res.json());
    }

    getAddons(menuId: number) {
        let params = {
            action: 'restaurantAddons',
            menuid: menuId
        };
        let query = queryString.stringify(params);

        return this.http.get(`${RESTAURANT_URL}?${query}`, {}).map(res => res.json());
    }

    getDeliveryZipcodes(resId: number) {
        let params = {
            action: 'restaurantDeliveryZipcode',
            resid: resId
        };
        let query = queryString.stringify(params);

        return this.http.get(`${RESTAURANT_URL}?${query}`, {}).map(res => res.json());
    }
}