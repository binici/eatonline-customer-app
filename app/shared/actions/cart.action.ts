import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { CartItem } from '../models/cart.model';

@Injectable()
export class CartActions {

    public static ADD_CARTITEM: string = 'ADD_CARTITEM';

    public static addCartItem(cartItem: CartItem): Action {
        return {
            type: CartActions.ADD_CARTITEM,
            payload: cartItem
        }
    }
}