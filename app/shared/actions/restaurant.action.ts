import { Injectable } from '@angular/core';
import { Action } from '@ngrx/store';
import { Restaurant } from '../models/restaurant.model';

@Injectable()
export class RestaurantActions {

    public static SET_RESTAURANT: string = 'SET_RESTAURANT';

    public static setRestaurant(restaurant: Restaurant): Action {
        return {
            type: RestaurantActions.SET_RESTAURANT,
            payload: restaurant
        }
    }
}